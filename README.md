# boot system

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 版本

`^`允许小版本更新

`~`允许版本补丁

> `@vue/cli`的`V4`版本对应`Webpack 4`
>
> warning `@vue/cli-plugin-eslint > eslint-loader@2.2.1` has incorrect peer dependency `eslint@>=1.6.0 <7.0.0` 经过实测，项目正常允许，该警告可忽略
>
> `sass-loader`的`V10`版本对应`Webpack 4`，`>=V10`的版本对应`Weback 5`
> warning `sass-loader@10.2.0` has unmet peer dependency `webpack@^4.36.0 || ^5.0.0` 该警告可忽略，`@vue/cli`的`V4`版本的 `dependencies` 包括了`webpack@^4.0.0`

```json
{
  "dependencies": {
    "@stomp/stompjs": "^6.1.2",
    "axios": "^0.24.0",
    "core-js": "^3.19.0",
    "element-plus": "^1.1.0-beta.24",
    "fuse.js": "^6.4.6",
    "json-bigint": "^1.0.0",
    "jwt-decode": "^3.1.2",
    "lodash": "^4.17.21",
    "normalize.css": "^8.0.1",
    "nprogress": "^0.2.0",
    "path-to-regexp": "^6.2.0",
    "remixicon": "^2.5.0",
    "screenfull": "^5.1.0",
    "utility-types": "^3.10.0",
    "vue": "^3.2.20",
    "vue-router": "^4.0.12",
    "vuex": "^4.0.2",
    "vuex-persistedstate": "^4.1.0"
  },
  "devDependencies": {
    "@types/json-bigint": "^1.0.1",
    "@types/lodash": "^4.14.176",
    "@types/node": "^15.3.0",
    "@types/nprogress": "^0.2.0",
    "@typescript-eslint/eslint-plugin": "^4.33.0",
    "@typescript-eslint/parser": "^4.33.0",
    "@vue/cli-plugin-babel": "~4.5.14",
    "@vue/cli-plugin-eslint": "~4.5.14",
    "@vue/cli-plugin-router": "~4.5.14",
    "@vue/cli-plugin-typescript": "~4.5.14",
    "@vue/cli-plugin-vuex": "~4.5.14",
    "@vue/cli-service": "~4.5.14",
    "@vue/compiler-sfc": "^3.0.0",
    "@vue/eslint-config-prettier": "^6.0.0",
    "@vue/eslint-config-typescript": "^7.0.0",
    "eslint": "^7.32.0",
    "eslint-plugin-prettier": "^3.4.0",
    "eslint-plugin-vue": "^7.20.0",
    "lint-staged": "^11.2.6",
    "prettier": "^2.4.1",
    "sass": "^1.32.13",
    "sass-loader": "10.2.0",
    "typescript": "~4.4.0"
  }
}
```

