module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/typescript/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'vue/multi-word-component-names': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-var': 2,
    'no-const-assign': 2,
    'no-constant-condition': 2,
    'no-empty-function': [2, { allow: ['arrowFunctions'] }],
    curly: [2, 'all'],
    'default-case': 2,
    eqeqeq: 2,
    semi: [2, 'always'],
    quotes: [1, 'single'],
    '@typescript-eslint/no-empty-function': [2, { allow: ['arrowFunctions'] }],
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'avoid',
        tabWidth: 2,
        printWidth: 120,
        singleQuote: true,
        bracketSpacing: true,
        vueIndentScriptAndStyle: true,
        bracketSameLine: true,
        htmlWhitespaceSensitivity: 'ignore',
      },
    ],
  },
};
