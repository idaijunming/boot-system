'use strict';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const defaultSettings = require('./src/settings.js');

// eslint-disable-next-line @typescript-eslint/no-var-requires
// const resolve = dir => require('path').join(__dirname, dir);

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { defineConfig } = require('@vue/cli-service');

// https://cli.vuejs.org/zh/config/
module.exports = defineConfig({
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: 1105,
    open: true,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_HTTP,
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '/api': '',
        },
      },
    },
  },
  configureWebpack: {
    name: defaultSettings.title,
    resolve: {
      // https://github.com/babel/babel/issues/8462
      // https://blog.csdn.net/qq_39807732/article/details/110089893
      // 如果确认需要node polyfill，设置resolve.fallback安装对应的依赖
      fallback: {
        // crypto: require.resolve('crypto-browserify'),
        path: require.resolve('path-browserify'),
        // url: require.resolve('url'),
        // buffer: require.resolve('buffer/'),
        // util: require.resolve('util/'),
        // stream: require.resolve('stream-browserify/'),
        // vm: require.resolve('vm-browserify')
      },
      // 如果确认不需要node polyfill，设置resolve.alias设置为false
      // alias: {
      //   crypto: false
      // }
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/assets/styles/index.scss";',
        // additionalData: '@import "@/assets/styles/index.scss";',
        // additionalData: '@use "@/assets/styles/index.scss" as *;',
        // additionalData: '@use "@/assets/styles/element-variables.scss" as *;',
        // additionalData: '@use "@/assets/styles/element-variables.scss" as *;@import "~@/assets/styles/index.scss";',
      },
    },
  },
});
