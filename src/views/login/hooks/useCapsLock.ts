import { ref } from 'vue';
import { ToRef } from '@/utils/vue';

export interface UseCapsLock {
  capslockTooltip: ToRef<boolean>;
  checkCapsLock: (e: KeyboardEvent) => void;
}

export const useCapsLock = (): UseCapsLock => {
  const capslockTooltip = ref(false);
  const checkCapsLock = (e: KeyboardEvent) => {
    const { key } = e;
    capslockTooltip.value = !!(key && key.length === 1 && key >= 'A' && key <= 'Z');
  };
  return {
    capslockTooltip,
    checkCapsLock,
  };
};
