import { ref } from 'vue';
import { ToRef } from '@/utils/vue';

export interface UsePasswordType {
  passwordType: ToRef<string>;
  changePasswordType: () => void;
}

export const usePasswordType = (): UsePasswordType => {
  const passwordType = ref('password');
  const changePasswordType = () => {
    if (passwordType.value === 'password') {
      passwordType.value = 'text';
    } else {
      passwordType.value = 'password';
    }
  };
  return {
    passwordType,
    changePasswordType,
  };
};
