export type ItemNode<T, E> = T & {
  id: E;
  parent?: E;
};

export type TreeNode<T> = T & { children: TreeNode<T>[] };

export const nest = <T, E>(items: ItemNode<T, E>[], id?: E): TreeNode<T>[] =>
  items.filter(item => item.parent === id).map(item => ({ ...item, children: nest(items, item.id) }));
