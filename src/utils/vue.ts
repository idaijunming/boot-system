import { ref } from 'vue';
import { Ref, ToRefs, UnwrapRef } from '@vue/reactivity';
import { ObjectUtil } from '@/utils/ObjectUtil';
import { DataObject } from '@/utils/types';

export type ToRef<T> = T extends Ref ? T : Ref<UnwrapRef<T>>;

export const refs = <T>(source: T): ToRefs<T> => {
  const target = {} as DataObject;
  const clone = ObjectUtil.clone(source) as unknown as DataObject;
  Object.getOwnPropertyNames(source).forEach(key => {
    target[key] = ref(clone[key]);
  });
  return target as ToRefs<T>;
};

export const assign = <T>(target: T[], source: T[]): void => {
  target.length = 0;
  source.forEach(v => target.push(v));
};

export const append = <T>(target: T[], source: T[]): void => {
  source.forEach(v => target.push(v));
};

/**
 * @deprecated
 **/
export type Reactive<T> = T extends Ref ? T : UnwrapRef<T>;
