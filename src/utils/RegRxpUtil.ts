export const isAllow = (A: string[], B: string[]): boolean => {
  const temp = B.filter(v => v.includes('*'));
  for (const a of A) {
    if (!B.includes(a)) {
      let flag = false;
      for (const b of temp) {
        if (new RegExp(b.replace('*', '\\w')).test(a)) {
          flag = true;
          break;
        }
      }
      if (!flag) {
        return false;
      }
    }
  }
  return true;
};
