export const random = (min: number, max: number): number => {
  return min + Math.random() * (max - min);
};

export const randomInt = (min: number, max: number): number => {
  return Math.floor(min + Math.random() * (max + 1 - min));
};

export const isFloat = (num: number): boolean => {
  return ~~num !== num;
};

export const chineseNumber = (num: number): string => {
  //将阿拉伯数字翻译成中文的大写数字
  const AA = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十'];
  const BB = ['', '十', '百', '仟', '萬', '億', '点', ''];
  const a = ('' + num).replace(/(^0*)/g, '').split('.');
  let k = 0;
  let result = '';
  for (let i = a[0].length - 1; i >= 0; i--) {
    // eslint-disable-next-line default-case
    switch (k) {
      case 0:
        result = BB[7] + result;
        break;
      case 4:
        if (!new RegExp('0{4}//d{' + (a[0].length - i - 1) + '}$').test(a[0])) {
          result = BB[4] + result;
        }
        break;
      case 8:
        result = BB[5] + result;
        BB[7] = BB[5];
        k = 0;
        break;
    }
    if (k % 4 === 2 && Number(a[0].charAt(i + 2)) !== 0 && Number(a[0].charAt(i + 1)) === 0) {
      result = AA[0] + result;
    }
    if (Number(a[0].charAt(i)) !== 0) {
      result = AA[Number(a[0].charAt(i))] + BB[k % 4] + result;
    }
    k++;
  }

  if (a.length > 1) {
    // 加上小数部分(如果有小数部分)
    result += BB[6];
    for (let i = 0; i < a[1].length; i++) {
      result += AA[Number(a[1].charAt(i))];
    }
  }
  if (result === '一十') {
    result = '十';
  }
  if (result.match(/^一/) && result.length === 3) {
    result = result.replace('一', '');
  }
  return result;
};

export const chineseMoney = (num: number): string => {
  const part = String(num).split('.');
  let newChar = '';
  //小数点前进行转化
  for (let i = part[0].length - 1; i >= 0; i--) {
    if (part[0].length > 10) {
      return '';
      //若数量超过拾亿单位，提示
    }
    let tempNewChar = '';
    const perChar = part[0].charAt(i);
    switch (perChar) {
      case '0':
        tempNewChar = '零' + tempNewChar;
        break;
      case '1':
        tempNewChar = '壹' + tempNewChar;
        break;
      case '2':
        tempNewChar = '贰' + tempNewChar;
        break;
      case '3':
        tempNewChar = '叁' + tempNewChar;
        break;
      case '4':
        tempNewChar = '肆' + tempNewChar;
        break;
      case '5':
        tempNewChar = '伍' + tempNewChar;
        break;
      case '6':
        tempNewChar = '陆' + tempNewChar;
        break;
      case '7':
        tempNewChar = '柒' + tempNewChar;
        break;
      case '8':
        tempNewChar = '捌' + tempNewChar;
        break;
      case '9':
        tempNewChar = '玖' + tempNewChar;
        break;
      default:
        throw new Error('无法转换');
    }
    switch (part[0].length - i - 1) {
      case 0:
        tempNewChar = tempNewChar + '元';
        break;
      case 1:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '拾';
        }
        break;
      case 2:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '佰';
        }
        break;
      case 3:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '仟';
        }
        break;
      case 4:
        tempNewChar = tempNewChar + '万';
        break;
      case 5:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '拾';
        }
        break;
      case 6:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '佰';
        }
        break;
      case 7:
        if (perChar !== '0') {
          tempNewChar = tempNewChar + '仟';
        }
        break;
      case 8:
        tempNewChar = tempNewChar + '亿';
        break;
      case 9:
        tempNewChar = tempNewChar + '拾';
        break;
      default:
        throw new Error('无法转换');
    }
    newChar = tempNewChar + newChar;
  }
  //小数点之后进行转化
  if (isFloat(num)) {
    if (part[1].length > 2) {
      // 小数点之后只保留两位
      part[1] = part[1].substr(0, 2);
    }
    for (let i = 0; i < part[1].length; i++) {
      let tempNewChar = '';
      const perChar = part[1].charAt(i);
      switch (perChar) {
        case '0':
          tempNewChar = '零' + tempNewChar;
          break;
        case '1':
          tempNewChar = '壹' + tempNewChar;
          break;
        case '2':
          tempNewChar = '贰' + tempNewChar;
          break;
        case '3':
          tempNewChar = '叁' + tempNewChar;
          break;
        case '4':
          tempNewChar = '肆' + tempNewChar;
          break;
        case '5':
          tempNewChar = '伍' + tempNewChar;
          break;
        case '6':
          tempNewChar = '陆' + tempNewChar;
          break;
        case '7':
          tempNewChar = '柒' + tempNewChar;
          break;
        case '8':
          tempNewChar = '捌' + tempNewChar;
          break;
        case '9':
          tempNewChar = '玖' + tempNewChar;
          break;
        default:
          throw new Error('无法转换');
      }
      if (i === 0) {
        tempNewChar = tempNewChar + '角';
      }
      if (i === 1) {
        tempNewChar = tempNewChar + '分';
      }
      newChar = newChar + tempNewChar;
    }
  }
  //替换所有无用汉字
  while (newChar.includes('零零')) {
    newChar = newChar.replace('零零', '零');
  }
  newChar = newChar.replace('零亿', '亿');
  newChar = newChar.replace('亿万', '亿');
  newChar = newChar.replace('零万', '万');
  newChar = newChar.replace('零元', '元');
  newChar = newChar.replace('零角', '');
  newChar = newChar.replace('零分', '');
  if (newChar.charAt(newChar.length - 1) === '元') {
    newChar = newChar + '整';
  }
  return newChar;
};
