export const TokenUtil = {
  set: (token: string): void => window.localStorage.setItem('token', token),

  remove: (): void => window.localStorage.removeItem('token'),

  get: (): string | null => window.localStorage.getItem('token'),
};
