import { createApp } from 'vue';
import App from './App.vue';
import { router } from './router';
import { store } from './store';

import ElementPlus from 'element-plus';
import 'normalize.css/normalize.css';
import 'element-plus/theme-chalk/index.css';
import '@/assets/styles/element-variables.scss';
// import '@/assets/styles/index.scss';
import SvgIcon from '@/components/icons/SvgIcon.vue';
import Authority from '@/components/Authority.vue';
import 'remixicon/fonts/remixicon.css';

const app = createApp(App);

app.config.errorHandler = (err, vm, info) => {
  console.error(vm, err, info);
};

app
  .use(router)
  .use(store)
  .use(ElementPlus, {
    size: store.getters['setting/size'],
  })
  .component('SvgIcon', SvgIcon)
  .component('Authority', Authority)
  .mount('#app');
