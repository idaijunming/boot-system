import { ref } from 'vue';

export interface UseId {
  getId: () => number;
}

export const useId = (): UseId => {
  const id = ref(1);
  const getId = (): number => {
    return id.value++;
  };
  return {
    getId,
  };
};
