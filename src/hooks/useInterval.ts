import { ref } from 'vue';

export const useInterval = (callback: () => void, milliseconds: number): (() => void) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const timeId = ref(null as any);

  const exec = () => {
    timeId.value = setInterval(() => {
      callback();
    }, milliseconds);
  };

  const clear = () => {
    if (timeId.value) {
      clearInterval(timeId.value);
      timeId.value = null;
    }
  };

  exec();

  return clear;
};
