import { ref } from 'vue';
import { ToRef } from '@/utils/vue';

export interface UseLoading {
  loading: ToRef<boolean>;
  setLoading: () => void;
}

export const useLoading = (): UseLoading => {
  const loading = ref(false);
  const setLoading = () => {
    loading.value = !loading.value;
  };
  return {
    loading,
    setLoading,
  };
};
