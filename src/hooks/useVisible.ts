import { ref } from 'vue';
import { Ref } from '@vue/reactivity';

export interface UseVisible {
  visible: Ref<boolean>;
  setVisible: () => void;
}

export const useVisible = (): UseVisible => {
  const visible = ref(false);
  const setVisible = () => {
    visible.value = !visible.value;
  };
  return {
    visible,
    setVisible,
  };
};
