import { onMounted, onUnmounted, ref } from 'vue';

export const useIntervalWithLifecycle = (callback: () => void, milliseconds: number): void => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const timeId = ref(null as any);

  const loop = () => {
    timeId.value = setInterval(() => {
      callback();
      loop();
    }, milliseconds);
  };

  onMounted(() => {
    loop();
  });
  onUnmounted(async () => {
    if (timeId.value) {
      clearInterval(timeId.value);
      timeId.value = null;
    }
  });
};
