import { reactive } from 'vue';
import { Ref } from '@vue/reactivity';
import { useVisible } from '@/hooks/useVisible';
import { ObjectUtil } from '@/utils/ObjectUtil';

export interface UseDialogForm<T> {
  form: T;
  resetForm: () => void;
  visible: Ref<boolean>;
  setVisible: () => void;
}

export const useVisibleForm = <T extends Record<string, unknown>>(defaultForm: T): UseDialogForm<T> => {
  const { visible, setVisible } = useVisible();
  const form = reactive<T>(ObjectUtil.clone(defaultForm)) as T;
  const resetForm = () => {
    Object.assign(form, defaultForm);
  };
  return {
    form,
    resetForm,
    visible,
    setVisible: () => {
      setVisible();
      resetForm();
    },
  };
};
