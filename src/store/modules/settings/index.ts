import variables from '@/assets/styles/element-variables.scss';
import defaultSettings from '@/settings';
import { Module } from 'vuex';
import { State } from '@/store';
import {
  CHANGE_SETTING,
  changeSetting,
  CLOSE_SIDEBAR,
  closeSidebar,
  device,
  fixedHeader,
  SET_SIZE,
  setSize,
  sidebar,
  size,
  tagsView,
  title,
  TOGGLE_DEVICE,
  TOGGLE_SIDEBAR,
  toggleDevice,
  toggleSidebar,
} from '@/store/modules/settings/pool';
import { DataObject } from '@/utils/types';

const {
  showSettings,
  title: defaultTitle,
  tagsView: defaultTagsView,
  fixedHeader: defaultFixedHeader,
  sidebarLogo,
} = defaultSettings;

export interface SettingState extends DataObject {
  theme: string;
  showSettings: boolean;
  title: string;
  tagsView: boolean;
  fixedHeader: boolean;
  sidebar: {
    logo: boolean;
    opened: boolean;
    withoutAnimation: boolean;
  };
  device: 'desktop' | 'mobile';
  size: 'large' | 'medium' | 'small' | 'mini';
}

export const settings: Module<SettingState, State> = {
  namespaced: true,
  state: (): SettingState => ({
    theme: variables.theme,
    // theme: 'green',
    showSettings,
    title: defaultTitle,
    tagsView: defaultTagsView,
    fixedHeader: defaultFixedHeader,
    sidebar: {
      logo: sidebarLogo,
      opened: true,
      withoutAnimation: false,
    },
    device: 'desktop',
    size: 'medium',
  }),
  getters: {
    [title]: state => state.title,
    [tagsView]: state => state.tagsView,
    [fixedHeader]: state => state.fixedHeader,
    [sidebar]: state => state.sidebar,
    [device]: state => state.device,
    [size]: state => state.size,
  },
  mutations: {
    [CHANGE_SETTING]: (state, setting: Partial<SettingState>) => {
      Object.assign(state, setting);
    },
    [TOGGLE_SIDEBAR]: state => {
      state.sidebar.opened = !state.sidebar.opened;
      state.sidebar.withoutAnimation = false;
    },
    [CLOSE_SIDEBAR]: (state, withoutAnimation) => {
      state.sidebar.opened = false;
      state.sidebar.withoutAnimation = withoutAnimation;
    },
    [TOGGLE_DEVICE]: (state, device) => {
      state.device = device;
    },
    [SET_SIZE]: (state, size) => {
      state.size = size;
    },
  },
  actions: {
    [changeSetting]: ({ commit }, setting: Partial<SettingState>) => {
      commit(CHANGE_SETTING, setting);
    },
    [toggleSidebar]({ commit }) {
      commit(TOGGLE_SIDEBAR);
    },
    [closeSidebar]({ commit }, withoutAnimation) {
      commit(CLOSE_SIDEBAR, withoutAnimation);
    },
    [toggleDevice]({ commit }, device) {
      commit(TOGGLE_DEVICE, device);
    },
    [setSize]({ commit }, size) {
      commit(SET_SIZE, size);
    },
  },
};
