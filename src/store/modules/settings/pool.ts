import { SettingState } from '@/store/modules/settings/index';
import { ActionMeta } from '@/store';

export const changeSetting = 'changeSetting';
export const toggleSidebar = 'toggleSidebar';
export const closeSidebar = 'closeSidebar';
export const toggleDevice = 'toggleDevice';
export const setSize = 'setSize';

export const title = 'title';
export const tagsView = 'tagsView';
export const fixedHeader = 'fixedHeader';
export const sidebar = 'sidebar';
export const device = 'device';
export const size = 'size';

export const CHANGE_SETTING = 'CHANGE_SETTING';
export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';
export const CLOSE_SIDEBAR = 'CLOSE_SIDEBAR';
export const TOGGLE_DEVICE = 'TOGGLE_DEVICE';
export const SET_SIZE = 'SET_SIZE';

export interface SettingActions {
  'settings/changeSetting': ActionMeta<Partial<SettingState>>;
  'settings/toggleSidebar': ActionMeta;
  'settings/closeSidebar': ActionMeta<boolean>;
  'settings/toggleDevice': ActionMeta<'desktop' | 'mobile'>;
  'settings/setSize': ActionMeta<'large' | 'medium' | 'small' | 'mini'>;
}

export interface SettingGetters {
  'settings/title': string;
  'settings/tagsView': boolean;
  'settings/fixedHeader': boolean;
  'settings/sidebar': {
    logo: boolean;
    opened: boolean;
    withoutAnimation: boolean;
  };
  'settings/device': 'desktop' | 'mobile';
  'settings/size': 'large' | 'medium' | 'small' | 'mini';
}
