import { CachedView, ViewState, VisitedView } from '@/store/modules/views/index';
import { ActionMeta } from '@/store';
import { _RouteLocationBase } from 'vue-router';

export const addView = 'addView';
export const addVisitedView = 'addVisitedView';
export const addCachedView = 'addCachedView';
export const delView = 'delView';
export const delVisitedView = 'delVisitedView';
export const delCachedView = 'delCachedView';
export const delOthersViews = 'delOthersViews';
export const delOthersVisitedViews = 'delOthersVisitedViews';
export const delOthersCachedViews = 'delOthersCachedViews';
export const delAllViews = 'delAllViews';
export const delAllVisitedViews = 'delAllVisitedViews';
export const delAllCachedViews = 'delAllCachedViews';
export const updateVisitedView = 'updateVisitedView';

export const visitedViews = 'visitedViews';
export const cachedViews = 'cachedViews';

export const ADD_VISITED_VIEW = 'ADD_VISITED_VIEW';
export const ADD_CACHED_VIEW = 'ADD_CACHED_VIEW';
export const DEL_VISITED_VIEW = 'DEL_VISITED_VIEW';
export const DEL_CACHED_VIEW = 'DEL_CACHED_VIEW';
export const DEL_OTHERS_VISITED_VIEWS = 'DEL_OTHERS_VISITED_VIEWS';
export const DEL_OTHERS_CACHED_VIEWS = 'DEL_OTHERS_CACHED_VIEWS';
export const DEL_ALL_VISITED_VIEWS = 'DEL_ALL_VISITED_VIEWS';
export const DEL_ALL_CACHED_VIEWS = 'DEL_ALL_CACHED_VIEWS';
export const UPDATE_VISITED_VIEW = 'UPDATE_VISITED_VIEW';

export interface ViewActions {
  'views/addView': ActionMeta<_RouteLocationBase>;
  'views/addVisitedView': ActionMeta<_RouteLocationBase>;
  'views/addCachedView': ActionMeta<_RouteLocationBase>;
  'views/delView': ActionMeta<_RouteLocationBase, ViewState>;
  'views/delVisitedView': ActionMeta<_RouteLocationBase, VisitedView[]>;
  'views/delCachedView': ActionMeta<_RouteLocationBase, CachedView[]>;
  'views/delOthersViews': ActionMeta<_RouteLocationBase, ViewState>;
  'views/delOthersVisitedViews': ActionMeta<_RouteLocationBase, VisitedView[]>;
  'views/delOthersCachedViews': ActionMeta<_RouteLocationBase, CachedView[]>;
  'views/delAllViews': ActionMeta<_RouteLocationBase, ViewState>;
  'views/delAllVisitedViews': ActionMeta<_RouteLocationBase, VisitedView[]>;
  'views/delAllCachedViews': ActionMeta<_RouteLocationBase, CachedView[]>;
  'views/updateVisitedView': ActionMeta<_RouteLocationBase>;
}

export interface ViewGetters {
  'views/visitedViews': VisitedView[];
  'views/cachedViews': CachedView[];
}
