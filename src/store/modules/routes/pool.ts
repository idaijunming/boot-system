import { ActionMeta } from '@/store';
import { Info } from '@/store/modules/account/info';
import { Route } from '@/router';

export const filterAsyncRoutes = 'filterAsyncRoutes';
export const resetRoutes = 'resetRoutes';

export const filterRoutes = 'filterRoutes';
export const menu = 'menu';
export const time = 'time';

export const RESET_STATE = 'RESET_STATE';
export const SET_STATE = 'SET_STATE';

export interface RouteActions {
  'routes/filterAsyncRoutes': ActionMeta<Info, Route[]>;
  'routes/resetRoutes': ActionMeta;
}

export interface RouteGetters {
  'routes/filterRoutes': Route[];
  'routes/menu': Route[];
  'routes/time': string;
}
