import { Client, messageCallbackType, StompHeaders } from '@stomp/stompjs';
import { ActionMeta } from '@/store';

export const connect = 'connect';
export const disconnect = 'disconnect';
export const subscribe = 'subscribe';
export const unsubscribe = 'unsubscribe';

export const client = 'client';
export const isConnect = 'isConnect';

export const RESET_STATE = 'RESET_STATE';
export const SET_CLIENT = 'SET_CLIENT';
export const ADD_SUBSCRIPTION = 'ADD_SUBSCRIPTION';
export const DEL_SUBSCRIPTION = 'DEL_SUBSCRIPTION';

export interface StompActions {
  'stomp/connect': ActionMeta<StompHeaders>;
  'stomp/disconnect': ActionMeta;
  'stomp/subscribe': ActionMeta<{
    destination: string;
    callback: messageCallbackType;
  }>;
  'stomp/unsubscribe': ActionMeta<string>;
}

export interface StompGetters {
  'stomp/client': Client;
  'stomp/isConnect': boolean;
}
