import { Info } from '@/store/modules/account/info';
import { LoginAccount } from '@/services/security/LoginAccount';
import { ActionMeta } from '@/store';

export const login = 'login';
export const logout = 'logout';
export const localValidate = 'localValidate';
export const remoteValidate = 'remoteValidate';
export const upgrade = 'upgrade';

export const token = 'token';
export const info = 'info';

export const RESET_STATE = 'RESET_STATE';
export const SET_STATE = 'SET_STATE';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_INFO = 'SET_INFO';

export interface AccountActions {
  'account/login': ActionMeta<LoginAccount, Info>;
  'account/logout': ActionMeta<boolean | undefined>;
  'account/localValidate': ActionMeta<(() => void) | undefined, boolean>;
  'account/remoteValidate': ActionMeta<(() => void) | undefined, boolean>;
  'account/upgrade': ActionMeta<unknown, Info>;
}

export interface AccountGetters {
  'account/token': string;
  'account/info': Info;
}
