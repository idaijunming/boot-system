/* eslint-disable */

import { Actions, Getters, State } from '@/store/index';
import { DispatchOptions, Store } from 'vuex';

declare module '@vue/runtime-core' {

  // provide typings for `this.$store`
  export interface ComponentCustomProperties {
    $store: Store<State>
  }
}


declare module 'vuex' {

  export interface Store<S> {
    readonly getters: {
      [P in keyof Getters]: Getters[P];
    };
    dispatch: Dispatch;
  }

  export interface Dispatch {
    <T extends keyof Actions, P = Actions[T]['payload'], R = Actions[T]['result']>(type: T, payload?: P, options?: DispatchOptions): Promise<R>;
  }
}
