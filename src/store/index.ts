import { createStore, Store } from 'vuex';
import { settings, SettingState } from '@/store/modules/settings';
import { account, AccountState } from '@/store/modules/account';
import { routes, RouteState } from '@/store/modules/routes';
import { views, ViewState } from '@/store/modules/views';
import { stomp, StompState } from '@/store/modules/stomp';
import createPersistedState from 'vuex-persistedstate';

import { AccountActions, AccountGetters } from '@/store/modules/account/pool';
import { RouteActions, RouteGetters } from '@/store/modules/routes/pool';
import { SettingActions, SettingGetters } from '@/store/modules/settings/pool';
import { StompActions, StompGetters } from '@/store/modules/stomp/pool';
import { ViewActions, ViewGetters } from '@/store/modules/views/pool';

export type ActionMeta<P = unknown, R = void> = {
  payload: P;
  result: R;
};

export interface State {
  account: AccountState;
  routes: RouteState;
  settings: SettingState;
  stomp: StompState;
  views: ViewState;
}

export interface Actions extends AccountActions, RouteActions, SettingActions, StompActions, ViewActions {}

export interface Getters extends AccountGetters, RouteGetters, SettingGetters, StompGetters, ViewGetters {
  settings: SettingState;
}

const persistedStatePlugin = createPersistedState<State>({
  key: 'boot',
  storage: window.localStorage,
  paths: ['account', 'settings'],
});

export const store: Store<State> = createStore({
  getters: {
    settings: state => state.settings as SettingState,
  },
  mutations: {},
  actions: {},
  modules: {
    account,
    routes,
    settings,
    stomp,
    views,
  },
  plugins: [persistedStatePlugin],
});
// console.log(store);
