module.exports = {
  /**
   * @type {boolean} true | false
   * @description Whether show the settings right-panel
   */
  showSettings: true,
  /**
   * @type {string}
   * @description 项目名称
   **/
  title: 'Boot System',
  /**
   * @type {boolean} true | false
   * @description Whether need tagsView
   */
  tagsView: true,
  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: true,
  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true,
  /**
   * @author 戴俊明 <idaijunming@163.com>
   * @description 用户默认头像地址
   * @date 2020/6/5 15:40
   **/
  defaultAvatar: '/openRoute/avatar.gif',
  /**
   * @type {boolean} true | false
   * @author 戴俊明 <idaijunming@163.com>
   * @description 是否开启 WebSocket连接
   * @date 2020/6/5 15:40
   **/
  websocket: true,
  /**
   * @type {boolean} true | false
   * @author 戴俊明 <idaijunming@163.com>
   * @description 是否开启 CDN
   * @date 2020/6/5 15:40
   **/
  isCDN: false,
};
