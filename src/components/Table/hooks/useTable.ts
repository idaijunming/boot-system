import { reactive } from 'vue';

export interface RowStatus {
  rowStatus: number;
  // 0：正常；1：添加状态；2：修改状态；3：删除状态
}

export interface RowEnabled {
  enabled: boolean;
}

export interface TableControl<T> {
  loading: boolean;
  tableData: Array<T>;
  prop: string | null;
  order: boolean | null;
  selection: T[] | null;
}

export interface UseTable<T> {
  tableControl: TableControl<T>;
  setSort: (column: { prop: string; order: string }) => void;
  setSelection: (selection: Array<T>) => void;
}

export const useTable = <T>(): UseTable<T> => {
  const tableControl = reactive({
    loading: false,
    tableData: [],
    prop: null,
    order: null,
    selection: null,
  }) as TableControl<T>;
  const setSort = (column: { prop: string; order: string }): void => {
    console.log(column);
    if (column.prop) {
      tableControl.prop = column.prop;
    } else {
      tableControl.prop = null;
    }
    switch (column.order) {
      case 'ascending':
        tableControl.order = true;
        break;
      case 'descending':
        tableControl.order = false;
        break;
      default:
        tableControl.order = null;
        break;
    }
  };
  const setSelection = (selection: Array<T>) => {
    tableControl.selection = selection;
  };
  return {
    tableControl,
    setSort,
    setSelection,
  };
};
