import { reactive } from 'vue';
import { RowStatus } from '@/components/Table/hooks/useTable';
import { ObjectUtil } from '@/utils/ObjectUtil';

export interface StatusControl<T> {
  status: number;
  // 0：正常；1：添加状态；2：修改状态
  data: (T & RowStatus) | null;
}

export interface UseStatus<T> {
  statusControl: StatusControl<T>;
  insertStatus: (row: T & RowStatus) => void;
  updateStatus: (row: T & RowStatus) => void;
  resetStatus: () => void;
}

export const useStatus = <T>(): UseStatus<T> => {
  const statusControl = reactive({
    status: 0,
    data: null,
  }) as StatusControl<T>;
  const insertStatus = (row: T & RowStatus) => {
    statusControl.status = 1;
    statusControl.data = ObjectUtil.clone(row);
  };
  const updateStatus = (row: T & RowStatus) => {
    statusControl.status = 2;
    statusControl.data = ObjectUtil.clone(row);
  };
  const resetStatus = () => {
    statusControl.status = 0;
    statusControl.data = null;
  };
  return {
    statusControl,
    insertStatus,
    updateStatus,
    resetStatus,
  };
};
