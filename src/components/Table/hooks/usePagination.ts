import { reactive } from 'vue';

export interface PageControl {
  total: number;
  current: number;
  pageSize: number;
  pages: number;
}

export interface UsePagination {
  pageControl: PageControl;
  setPageSize: (size: number) => boolean;
  setCurrent: (current: number) => boolean;
  getPageNum: (index: number) => number;
}

export const usePagination = (): UsePagination => {
  const pageControl = reactive({
    total: 0,
    current: 1,
    pageSize: 10,
    pages: 0,
  });
  const setPageSize = (size: number): boolean => {
    if (pageControl.pageSize === size) {
      return false;
    }
    pageControl.pageSize = size;
    return true;
  };
  const setCurrent = (current: number): boolean => {
    if (pageControl.current === current) {
      return false;
    }
    pageControl.current = current;
    return true;
  };
  const getPageNum = (index: number): number => {
    return Math.ceil(index / pageControl.pageSize);
  };
  return {
    pageControl,
    setPageSize,
    setCurrent,
    getPageNum,
  };
};
