interface Settings {
  showSettings: boolean,
  title: string,
  tagsView: boolean,
  fixedHeader: boolean,
  sidebarLogo: boolean,
  defaultAvatar: string,
  websocket: boolean,
  isCDN: boolean
}

declare const defaultSettings: Settings;

export default defaultSettings;
