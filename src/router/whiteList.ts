/**
 * @author 戴俊明 <idaijunming@163.com>
 * @description 白名单
 * @date 2021/2/22 21:30
 **/
export const whiteList = ['/login', '/register'];
