import { createRouter, createWebHistory, RouteMeta, Router, RouteRecordRaw } from 'vue-router';
import { decorateRouter } from '@/router/decorateRouter';
import { decorateAsyncRoutes, decorateRoutes } from '@/router/decorateRoutes';
import { mergeRoutes } from '@/router/authority';
import { DeepRequired } from '@/utils/types';

export type Route = RouteRecordRaw & {
  path: string;
  redirect?: string;
  name: string;
  meta: DeepRequired<RouteMeta>;
  children?: Route[];
};

const Layout = () => import(/* webpackChunkName: "layout" */ '@/layout/index.vue');
const AppMain = () => import(/* webpackChunkName: "layout" */ '@/layout/components/AppMain.vue');

export const constantRoutes: Route[] = decorateRoutes([
  {
    path: '/',
    name: 'home',
    component: Layout,
    children: [
      {
        path: '/home',
        meta: {
          title: '主页',
          icon: 'ri-home-3-line',
          affix: true,
        },
        component: () => import(/* webpackChunkName: "layout" */ '@/views/home/index.vue'),
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录',
      noCache: true,
      hidden: true,
    },
    component: () => import(/* webpackChunkName: "base" */ '../views/login/index.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    meta: {
      title: '注册',
      noCache: true,
      hidden: true,
    },
    component: () => import(/* webpackChunkName: "base" */ '../views/register/index.vue'),
  },
  {
    path: '/redirect/:path(.*)',
    name: 'redirect',
    meta: {
      title: 'redirect',
      noCache: true,
      hidden: true,
    },
    component: () => import(/* webpackChunkName: "base" */ '@/views/redirect/index.vue'),
  },
  {
    path: '/profile',
    component: Layout,
    meta: {
      title: 'Profile',
      hidden: true,
    },
    children: [
      {
        path: '/account',
        component: () => import(/* webpackChunkName: "layout" */ '@/views/profile/index.vue'),
        meta: { title: 'Account', icon: 'ri-user-line' },
      },
    ],
  },
  // {
  //   path: '/test',
  //   name: 'test',
  //   component: Layout,
  //   children: [
  //     {
  //       path: '/ui',
  //       meta: {
  //         title: 'UI测试'
  //       },
  //       component: () =>
  //         import(/* webpackChunkName: "test" */ '@/views/test/ui.vue')
  //     }
  //   ]
  // }
]);

export const errorRoutes: Route[] = decorateRoutes([
  {
    path: '/401',
    component: () => import(/* webpackChunkName: "error" */ '@/views/error-page/401.vue'),
    meta: { title: '401', hidden: true },
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import(/* webpackChunkName: "error" */ '@/views/error-page/404.vue'),
    meta: { title: '404', hidden: true },
  },
]);

export const asyncRoutes: Route[] = decorateAsyncRoutes([
  {
    path: '/system',
    name: 'system',
    meta: {
      title: '系统管理',
      icon: 'ri-settings-4-line',
    },
    component: Layout,
    children: [
      {
        path: '/user',
        meta: {
          title: '用户管理',
        },
        component: () => import(/* webpackChunkName: "system" */ '@/views/system/User/table.vue'),
      },
      {
        path: '/authority',
        meta: {
          title: '权限管理',
        },
        component: AppMain,
        children: [
          {
            path: '/tree',
            meta: {
              title: '权限树图',
            },
            component: () => import(/* webpackChunkName: "system" */ '@/views/system/Authority/tree.vue'),
          },
          {
            path: '/list',
            meta: {
              title: '权限列表',
            },
            component: () => import(/* webpackChunkName: "system" */ '@/views/system/Authority/table.vue'),
          },
        ],
      },
      {
        path: '/resource',
        meta: {
          title: '资源管理',
        },
        component: () => import(/* webpackChunkName: "system" */ '@/views/system/Resource/table.vue'),
      },
      {
        path: '/authority-resource',
        meta: {
          title: '权限资源映射',
        },
        component: () => import(/* webpackChunkName: "system" */ '@/views/system/AuthorityResource/table.vue'),
      },
    ],
  },
]);

export const defaultRoutes: Route[] = mergeRoutes(constantRoutes, errorRoutes);

// console.log(constantRoutes);
// console.log(errorRoutes);
// console.log(asyncRoutes);
// console.log(defaultRoutes);

export const router: Router = decorateRouter(
  createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes: defaultRoutes,
  })
);
