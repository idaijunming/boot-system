import 'vue-router';
import { Authority } from '@/store/modules/account/info';

declare module 'vue-router' {
  export interface RouteMeta {
    /**
     * @description the name show in sidebar and breadcrumb (recommend set)
     * @date 2021/2/17 21:32
     **/
    title: string;
    /**
     * @description the icon show in the sidebar
     * @date 2021/2/17 21:32
     **/
    icon?: string;
    /**
     * @description if set true, the tag will affix in the tags-view(default is false)
     * @date 2021/2/17 21:32
     **/
    affix?: boolean;
    /**
     * @description if set true, item will not show in the sidebar(default is false)
     * @date 2021/2/17 21:32
     **/
    hidden?: boolean;
    /**
     * @description if set true, the page will no be cached(default is false)
     * @date 2021/2/17 21:32
     **/
    noCache?: boolean;
    /**
     * @description if set false, the item will hidden in breadcrumb(default is true)
     * @date 2021/2/17 21:33
     **/
    breadcrumb?: boolean;
    /**
     * @deprecated
     * @description control the page authority (you can set multiple authorities)
     * @date 2021/2/17 21:31
     **/
    authority?: Partial<Authority>;
    authorities?: string[]
    /**
     * @description control the order of the menu (from largest to smallest)
     * @date 2021/2/26 17:05
     **/
    order?: number
  }
}
