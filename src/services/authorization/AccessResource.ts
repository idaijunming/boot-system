import { DataResponse, DefaultModel } from '../core';
import { AssociationService } from '../core/DataService';
import { Access } from '@/services/authorization/Access';
import { Resource } from '@/services/authorization/Resource';
import { DataObject } from '@/utils/types';

export interface AccessResource extends DefaultModel {
  accessId: string;
  resourceId: string;
}

class AccessResourceService extends AssociationService<AccessResource, Access, Resource, string, string, string> {
  protected create(): void {
    this.initTokenInstance('/api/access-resource');
  }

  searchPathAuthority = (path: string): Promise<DataResponse<string[]>> =>
    this.request({
      method: this.method.POST,
      url: 'authority',
      data: {
        path,
      },
    });

  searchPathAuthorities = (paths: string[]): Promise<DataResponse<DataObject<string[]>>> =>
    this.request({
      method: this.method.POST,
      url: 'authorities',
      data: paths,
    });

  getMid(model: AccessResource): string {
    return model.accessId;
  }

  getNid(model: AccessResource): string {
    return model.resourceId;
  }

  model(mid?: string, nid?: string): Partial<AccessResource> {
    return {
      accessId: mid,
      resourceId: nid,
    };
  }
}

export const accessResourceService = new AccessResourceService();
