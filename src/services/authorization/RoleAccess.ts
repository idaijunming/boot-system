import { DefaultModel } from '../core';
import { DataService } from '../core/DataService';

export interface RoleAccess extends DefaultModel {
  roleId: string;
  accessId: string;
}

class RoleAccessService extends DataService<RoleAccess, string> {
  protected create(): void {
    this.initTokenInstance('/api/role-access');
  }
}

export const roleAccessService = new RoleAccessService();
