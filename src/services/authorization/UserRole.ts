import { DefaultModel } from '../core';
import { DataService } from '../core/DataService';

export interface UserRole extends DefaultModel {
  userId: string;
  roleId: string;
}

class UserRoleService extends DataService<UserRole, string> {
  protected create(): void {
    this.initTokenInstance('/api/user-role');
  }
}

export const userRoleService = new UserRoleService();
