import { DefaultModel } from '../core';
import { DataService } from '../core/DataService';

export interface Role extends DefaultModel {
  code: string;
  remark: string;
  parent: string[];
}

class RoleService extends DataService<Role, string> {
  protected create(): void {
    this.initTokenInstance('/api/role');
  }
}

export const roleService = new RoleService();
