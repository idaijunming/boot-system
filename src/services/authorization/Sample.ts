import { TimeModel } from '../core';
import { TimeDataService } from '../core/TimeDataService';

import { DateUtil } from '@/utils/DateUtil';
import { Overwrite } from '@/utils/types';

export interface Sample extends TimeModel {
  username: string;
  gender: boolean;
  nickname: string;
  phone: string;
  email: string;
  avatar: string;
  status: number;
}

export type SampleUpdate = Overwrite<Sample, 'createTime' | 'updateTime', string>;

class SampleService extends TimeDataService<Sample, 'createTime' | 'updateTime', SampleUpdate> {
  protected create(): void {
    this.initTokenInstance('user');
  }

  protected format(o: Sample): SampleUpdate {
    return DateUtil.formatObject(o, ['createTime', 'updateTime']);
  }

  protected time(o: SampleUpdate): Sample {
    return DateUtil.timeObject(o, ['createTime', 'updateTime']);
  }
}

export const sampleService = new SampleService();
