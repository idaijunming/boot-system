import { DataResponse, DefaultModel, HttpModel } from '../core';
import { DataService } from '../core/DataService';
import json from 'json-bigint';

export interface User extends DefaultModel {
  username: string;
  gender: boolean;
  nickname: string;
  phone: string;
  email: string;
  avatar: string;
  status: number;
}

class UserService extends DataService<User, string> {
  protected create(): void {
    this.initTokenInstance('/api/user');
  }

  resetPassword = (id: string): Promise<DataResponse<void>> =>
    this.request<HttpModel<void>>({
      method: this.method.POST,
      url: `/id/${id}`,
    });

  test1 = (list: bigint[], useStrict = false): Promise<DataResponse<User[]>> =>
    this.request({
      method: this.method.POST,
      url: '/list/id',
      data: list,
      transformRequest: [
        function (data: unknown, headers: unknown) {
          console.log(data);
          console.log(json.stringify(data));
          console.log(headers);
          return json.stringify(data);
        },
      ],
      // data: list.map(v => BigInt(v)),
      headers: {
        useStrict: String(useStrict),
        'Content-Type': 'application/json;charset=UTF-8',
      },
      responseType: 'json',
    });

  test2 = (list: string, useStrict = false): Promise<DataResponse<User[]>> =>
    this.request({
      method: this.method.POST,
      url: '/list/id',
      data: list,
      // data: list.map(v => BigInt(v)),
      headers: {
        useStrict: String(useStrict),
        'Content-Type': 'application/json;charset=UTF-8',
      },
    });
}

export const userService = new UserService();
