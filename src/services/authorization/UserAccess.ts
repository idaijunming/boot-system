import { DefaultModel } from '../core';
import { AssociationService } from '../core/DataService';
import { Access } from '@/services/authorization/Access';
import { User } from '@/services/authorization/User';

export interface UserAccess extends DefaultModel {
  userId: string;
  accessId: string;
}

class UserAccessService extends AssociationService<UserAccess, User, Access, string, string, string> {
  protected create(): void {
    this.initTokenInstance('/api/user-access');
  }

  getMid(model: UserAccess): string {
    return model.userId;
  }

  getNid(model: UserAccess): string {
    return model.accessId;
  }

  model(mid?: string, nid?: string): Partial<UserAccess> {
    return {
      userId: mid,
      accessId: nid,
    };
  }
}

export const userAccessService = new UserAccessService();
