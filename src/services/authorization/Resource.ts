import { DefaultModel } from '../core';
import { DataService } from '../core/DataService';

export interface Resource extends DefaultModel {
  remark: string;
  type: number;
  content: string;
  method: number;
}

class ResourceService extends DataService<Resource, string> {
  protected create(): void {
    this.initTokenInstance('/api/resource');
  }
}

export const resourceService = new ResourceService();
