import { DataResponse, DefaultModel, HttpModel } from '../core';
import { DataService } from '../core/DataService';

export interface Access extends DefaultModel {
  code: string;
  remark: string;
  parent?: string;
}

class AccessService extends DataService<Access, string> {
  protected create(): void {
    this.initTokenInstance('/api/access');
  }

  selectByCodes = (authorities: string[]): Promise<DataResponse<Access[]>> =>
    this.request<HttpModel<Access[]>>({
      method: this.method.POST,
      url: '/list/code',
      data: authorities,
    });
}

export const accessService = new AccessService();
