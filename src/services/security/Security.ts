import { DataResponse, Service } from '../core';

class SecurityService extends Service {
  protected create(): void {
    this.initTokenInstance('/api/security');
  }

  authentication = (): Promise<DataResponse<void>> =>
    this.request({
      method: this.method.POST,
      url: '/authentication',
    });

  authorization = (authorities: string[]): Promise<DataResponse<void>> =>
    this.request({
      method: this.method.POST,
      url: '/authorization',
      data: authorities,
    });
}

export const securityService = new SecurityService();
